<?php

$router = $di->getRouter();

$router->handle($_SERVER['REQUEST_URI']);
$router->add("/registro", "API::registro", ["POST"]);
$router->add("/login", "API::login", ["POST"]);
