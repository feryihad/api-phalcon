<?php

use Phalcon\Http\Response;
use Phalcon\Http\Request;
use Phalcon\Mvc\Controller;

class APIController extends Controller
{
    public function registroAction()
    {
      $this->view->disable();
      $response = new Response();
      $request = new Request();

      if ($request->isPost()) {

          $data = $request->getJsonRawBody();
          $user = new Users();
          $user->nombre = $data->nombre;
          $user->email = $data->email;
          $user->estatus = '1';
          $user->password = md5($data->password);
          $user->fechaNacimiento = $data->fechanacimiento;

          if ($user->save() === false) {
            $response->setStatusCode(400, 'Error en variables');
            $response->setJsonContent(["status" => false, "error" => "Error por parte del usuario"]);
          } else {
            $response->setStatusCode(200, 'OK');
            $response->setJsonContent(["status" => true, "error" => false, "data" => $data]);
          }


      } else {
          $response->setStatusCode(405, 'Method Not Allowed');
          $response->setJsonContent(["status" => false, "error" => "Method Not Allowed"]);
      }
      $response->send();
    }
    public function loginAction()
    {
        $this->view->disable();
        $response = new Response();
        $request = new Request();
        $data = $request->getJsonRawBody();

        if ($request->isPost()) {
            $email = $data->email;
            $password = md5($data->password);
            $user = Users::findFirst([
                'conditions' => 'email = ?1 and password = ?2',
                'bind' => [
                    1 => $email,
                    2 => $password,
                ]
            ]);
            if ($user) {
                $returnData = [
                    "id" => $user->id,
                    "nombre" => $user->nombre,
                    "email" => $email,
                ];
                $response->setStatusCode(200, 'OK');
                $response->setJsonContent(["status" => true, "error" => false, "message" => "Logeado. :)", "data" => $returnData ]);

            } else {
                $response->setStatusCode(400, 'Bad Request');
                $response->setJsonContent(["status" => false, "error" => "Invalid Email and Password."]);
            }
        } else {
            $response->setStatusCode(405, 'Method Not Allowed');
            $response->setJsonContent(["status" => false, "error" => "Method Not Allowed"]);
        }
        $response->send();
    }
}
